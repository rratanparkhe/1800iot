var awsIot = require('aws-iot-device-sdk');
var MongoClient = require('mongodb').MongoClient;
var express = require('express');
var app = express();

var mongojs = require('mongojs');

var MONGO_URL = 'mongodb://admin:admin123@ds139138.mlab.com:39138/sensoralert';


var database = mongojs(MONGO_URL, ['sensoralert'])


// MongoClient.connect(MONGO_URL, { server: { 
//     // sets how many times to try reconnecting
//     reconnectTries: Number.MAX_VALUE,
//     // sets the delay between every retry (milliseconds)
//     reconnectInterval: 1000 
//     } 
// }, (err, db) => {
//     if (err) {
//         return console.log(err);
//     }
//     database = db;
//     // console.log('db connected', db);
// });

var device = awsIot.device({
    keyPath: '../Certificates/d9ea11dfbd-private.pem.key',
    certPath: '../Certificates/d9ea11dfbd-certificate.pem.crt',
    caPath: '../Certificates/rootCA.pem',
    clientId: 'raspberryPi',
    port: 8883,
    region: 'us-west-2',
    host: 'a3087nu5ze6pl5.iot.us-west-2.amazonaws.com'
});

device.on('connect', function () {
    device.subscribe('$aws/things/raspberryPi/shadow/get/accepted');
    // device.subscribe('$aws/things/raspberryPi/shadow/get/rejected');
    // Publish an empty packet to topic `$aws/things/raspberryPi/shadow/get`
    // to get the latest shadow data on either `accepted` or `rejected` topic
    device.publish('$aws/things/raspberryPi/shadow/get', '');
});

device.on('message', function (topic, payload) {
    payload = JSON.parse(payload.toString());
    console.log('message from ', topic, JSON.stringify(payload, null, 4));
    // Do something with db here, like inserting a record
    database.collection('alerts').insert({
            'createdAt': payload.state.desired.date,
            'deviceId': payload.state.desired.sensorId
        },
        function (err, res) {
            if (err) {
                return console.log(err);
            }
            // Success
        }
    );
});

var path = require('path');

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
    //__dirname : It will resolve to your project folder.
});


app.get('/data', function (req, res) {
    console.log(req.query.sensorId);
    database.collection('alerts').find({
        'deviceId': parseInt(req.query.sensorId)
    }).toArray(function (err, result) {
        if (err) {
            res.json(err);
            return;
        }
        res.json(result);
    });
});

app.listen(process.env.PORT || 3000);

console.log('Running at Port 3000');