var statistics = require('math-statistics');
var usonic = require('mmm-usonic');
var Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
var awsIot = require('aws-iot-device-sdk');
var thingShadow = awsIot.thingShadow({
    	keyPath: '../Certificates/d9ea11dfbd-private.pem.key',
    	certPath: '../Certificates/d9ea11dfbd-certificate.pem.crt',
    	caPath: '../Certificates/rootCA.pem',
    	clientId: 'raspberryPi',
    	region: 'us-west-2',
	host: 'a3087nu5ze6pl5.iot.us-west-2.amazonaws.com',
	port: 8883
});
var green = new Gpio(22, 'out'), //use declare variables for all the GPIO output pins
	red = new Gpio(17, 'out'),
	yellow = new Gpio(27, 'out'),
	buzzer = new Gpio(10, 'out'),
	leds = [green, red, yellow],
	currentStatus = '';

thingShadow.on('connect', function() {
    	console.log('Connected.');
	usonic.init(function(error){
		if(error){
			process.stdout.write(error +'\n');
		}
		else {
			init({
			    echoPin: 24, //Echo pin
			    triggerPin: 18, //Trigger pin
			    timeout: 3000, //Measurement timeout in µs
			    delay: 60, //Measurement delay in ms
			    rate: 5 //Measurements per sample
			});
			thingShadow.register('raspberryPi', {}, function() {
				setTimeout(sendData, 15000);
			});
		}
	});
});

function fetchData() {
	return {
		'sensorId': 1, 
		'date' : new Date()
	};
}

function sendData() {
	var sensorState = {
	'state': {
		'desired': fetchData()
		}
	};
	var isUpdated = thingShadow.update('raspberryPi', sensorState);
	if (isUpdated === null) {
		console.log('shadow failed');
	} else {
		console.log('success');
	}
}

thingShadow.on('status', function(thingName, stat, clientToken, stateObject) {
    console.log('received ' + stat + ' on ' + thingName + ':', stateObject);
});

thingShadow.on('delta', function(thingName, stateObject) {
    console.log('received delta on ' + thingName + ':', stateObject);
});

thingShadow.on('timeout', function(thingName, clientToken) {
    console.log('received timeout on ' + thingName + ' with token:', clientToken);
});
 
var init = function(config) {
    var sensor = usonic.createSensor(config.echoPin, config.triggerPin, config.timeout);
    var distances;
 
    (function measure() {
        if (!distances || distances.length === config.rate) {
            if (distances) {
                print(distances);
            }
 
            distances = [];
        }
 
        setTimeout(function() {
            	distances.push(sensor());
		measure();
        }, config.delay);
    }());
};
 
var print = function(distances) {
    var distance = statistics.median(distances);
 
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
 
    if (distance < 0) {
        process.stdout.write('Error: Measurement timeout.\n');
    } else {
        process.stdout.write('Distance: ' + distance.toFixed(2) + ' cm');
	leds.forEach(function(currentValue) { //for each item in array
		currentValue.writeSync(0); //turn off LED
  	});
	if (distance < 31) {
		red.writeSync(1);
		buzzer.writeSync(1);
		currentStatus=0;
		sendData();
	} else if (distance < 61 && distance > 31) {
		yellow.writeSync(1);
		buzzer.writeSync(0);
		currentStatus=1;
	} else {
		green.writeSync(1);
		buzzer.writeSync(0);
		currentStatus=2;
	}
    }
};

function unexportOnClose() { //function to run when exiting program
  leds.forEach(function(currentValue) { //for each LED
    currentValue.writeSync(0); //turn off LED
    currentValue.unexport(); //unexport GPIO
  });
};

process.on('SIGINT', unexportOnClose); //function to run when user closes using ctrl+cc